What makes a "good" iOS App?  
Simplicity, usability, no-ads

Motivation:  
I am really passionate about things I do in church/worship team,
so I want to help my team with schedules arranging accordingly with
members availability

Ideas:  
An app that can:
- add services that can have recurrence
- add members to the group
- add impediments to members when they can not participate
- generate schedule based on services dates and members availability

KISS - Keep it simple stupid:  

Don`t worry if the app already exists:  

Brainstorm:  

